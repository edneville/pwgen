#[cfg(test)]
mod test {
    use pwgen::*;

    #[test]
    fn test_acceptable_lowercase() {
        let test = "abc";
        let test_bad = "ABC";

        let set = Some(PwClass::Lower as u32);

        assert_eq!(valid_word(&test, set), true);
        assert_eq!(valid_word(&test_bad, set), false);
    }

    #[test]
    fn test_acceptable_uppercase() {
        let test = "ABC";
        let test_bad = "abc";

        let set = Some(PwClass::Upper as u32);

        assert_eq!(valid_word(&test, set), true);
        assert_eq!(valid_word(&test_bad, set), false);
    }

    #[test]
    fn test_acceptable_numeric() {
        let test = "abc1";
        let test_bad = "abcABC";

        let set = Some(PwClass::Num as u32);

        assert_eq!(valid_word(&test, set), true);
        assert_eq!(valid_word(&test_bad, set), false);
    }

    #[test]
    fn test_acceptable_extended() {
        let test_bad = "abcABC";

        let set = Some(PwClass::Ext as u32);

        for j in '!'..='/' {
            assert_eq!(valid_word(j.to_string().as_ref(), set), true);
        }

        assert_eq!(valid_word(&test_bad, set), false);
    }
}
