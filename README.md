# pwgen

Generate passwords from random characters or words and optionally show their cryptographic hash.

The default generated password set is copy/paste friendly without extended characters that would break the default copy selection you get when double-clicking a word. They also are don't break quotation strings (quote marks, double quotes or backticks).

# installing

```
git clone https://gitlab.com/edneville/pwgen.git
cd pwgen
cargo build --release
```

please or sudo
```
please install -m0755 -o0 -g0 target/release/pwgen /usr/local/bin/
```

# modifiers

`lowercase`, `uppercase`, `numeric` and `extended` will set requirements on the passwords.

# word lists

If you want a password generated from words rather than a mixture of letters and numbers, use the `words` option, which by default uses the file `/usr/share/dict/words`. Use `wordsfile` to specify a different list.

# cryptographic hash

The `--md5`, `--bcrypt`, `--des`, `--sha1`, `--sha256` and `--sha512` options will print the cryptographic hash which can be used instead of storing the password in some systems.

The hash output can be used with `useradd`/`usermod` or `.htaccess`:

```
LINE=`pwgen --number 1 --sha256`
PW="${LINE% *}"
HASH="${LINE##* }"
USR=newuser
useradd -m -p "$HASH" -s /bin/bash $USR
echo "Password for $USR  is $PW"
```

Or issue a new password to an existing user with `usermod`:

```
LINE=`pwgen --number 1 --sha256`
PW=`echo "$LINE" | sed -e 's/ .*//g'`
HASH=`echo "$LINE" | sed -e 's/.* //g'`
USR=newuser
usermod -p "$HASH" $USR
echo "Password for $USR is now $PW"
```

